<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//  login route
Route::get('/', function () {
    return view('admin.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//  Admin auth
Route::match(['get', 'post'], '/Admin/Auth', 'AdminController@authentication');

Route::group(['middleware' => ['auth']], function () {

    //  middelware Authentication
    Route::match(['get', 'post'], '/company/dashboard', [CompanyController::class, 'dashboard']);

    Route::resource('companies', 'CompanyController');

    Route::resource('employees', 'EmployeeController');
});
//  Logout Middelware
Route::get('/logout', 'AdminController@logout');
