@extends('admin.layouts.master')
@section('title','Add Company')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="header-icon">
          <i class="fa fa-users"></i>
       </div>
       <div class="header-title">
          <h1>Add Company</h1>
          <small>Add Company</small>
       </div>
    </section>
    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
    <!-- Main content -->
    <section class="content">
     
       <div class="row">
          <!-- Form controls -->
          <div class="col-sm-12">
             <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                   <div class="btn-group" id="buttonlist"> 
                   <a class="btn btn-add " href="{{route('companies.index')}}"> 
                      <i class="fa fa-eye"></i>  View companies </a>  
                   </div>
                </div>
                <div class="panel-body">
                  <form action="{{ route('companies.store') }}" method="post" enctype="multipart/form-data">
                     @csrf
                     <div class="form-group">
                       <label for="">Company Name :</label>
                     <input type="text" name="name" id="" class="form-control" placeholder="Enter your name" value="{{old('name')}}" >
                     </div>
                     <div class="form-group">
                       <label for="">Email :</label>
                       <input type="text" name="email" id="" class="form-control" placeholder="Enter your email" value="{{old('email')}}" >
                     </div>
                    
                     <div class="form-group">
                       <label for="">Company Logo :</label>
                       <input type="file" name="image" id="" class="form-control"  >
                        
                     </div>
                     <div class="form-group">
                        <label for="">Website Url :</label>
                        <input type="text" name="web_url" id="" class="form-control" placeholder="Enter Url" value="{{old('web_url')}}" >
                      </div>
                     
         
                     <br>
                     <input type="submit" value="Submit Form" class="btn btn-info btn-lg">
         
                 </form>
                </div>
             </div>
          </div>
       </div>
    </section>
    <!-- /.content -->
 </div>
@endsection