@extends('admin.layouts.master')
@section('title','Edit Product')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="header-icon">
          <i class="fa fa-users"></i>
       </div>
       <div class="header-title">
          <h1>Edit Company</h1>
          <small>Edit Company</small>
       </div>
    </section>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
        
    <!-- Main content -->
    <section class="content">
       <div class="row">
          <!-- Form controls -->
          <div class="col-sm-12">
             <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                   <div class="btn-group" id="buttonlist"> 
                      <a class="btn btn-add " href="{{url('companies')}}"> 
                      <i class="fa fa-eye"></i>  View Companies </a>  
                   </div>
                </div>
                <div class="panel-body">
                    <form action="{{ route('companies.update',$company->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label for="">Company Name :</label>
                        <input type="text" name="name" id="" class="form-control" placeholder="Enter your name" value="{{$company->name}}" >
                        </div>
                        <div class="form-group">
                          <label for="">Email :</label>
                          <input type="text" name="email" id="" class="form-control" placeholder="Enter your email" value="{{$company->email}}" >
                        </div>
                        <div class="form-group">
                           <label for="">Website Url :</label>
                           <input type="text" name="web_url" id="" class="form-control" placeholder="Enter Url" value="{{$company->website}}" >
                         </div>
                        <div class="form-group">
                          <label for="">Upload  Image :</label>
                          <input type="file" name="image" id="" class="form-control"  >     
                        </div>
                       
                        <div>
                            <h5 class=" text-black">Company Logo Picture</h5>
                            <img src="{{ url('storage/app/public/company',$company->logo) }}" alt="" id="editimg" style="width: 90px; height:60px;  " class="rounded">
                          </div>
                         
                        <div class="form-group">
                          <input type="hidden" name="my_image" value="{{$company->logo}}"  >     
                        </div>
                        
          
                        <br>
                        <input type="submit" value="Submit Form" class="btn btn-info btn-lg">
                
                    </form>
                </div>
             </div>
          </div>
       </div>
    </section>
    <!-- /.content -->
 </div>
@endsection