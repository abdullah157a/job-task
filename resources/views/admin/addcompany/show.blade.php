@extends('admin.layouts.master')
@section('title',' Company Details')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="header-icon">
          <i class="fa fa-users"></i>
       </div>
       <div class="header-title">
          <h1>Company Detials</h1>
          <small>Company Detials</small>
       </div>
    </section>
    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
    <!-- Main content -->
    <section class="content">
     
       <div class="row">
          <!-- Form controls -->
          <div class="col-sm-12">
             <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                   <div class="btn-group" id="buttonlist"> 
                   <a class="btn btn-add " href="{{route('companies.index')}}"> 
                      <i class="fa fa-eye"></i> Back To Company View </a>  
                   </div>
                </div>
                <div class="panel-body">
                    <div class="jumbotron bg-info text-center">
                        <h3 class=" p-3">Welcome To Company Detail</h3>
                        <img src="{{ url('storage/app/public/company',$company->logo) }}" alt="" id="showimg" class="rounded-circle" style="width: 100px; height:100px;">
                        <div class="container">
                            <Div class="" style="background-color: grey; border-radius: 5px; padding: 20px; margin-top: 5px; border: 2px solid red;">
                                <h2>Company Name : {{$company->name}}</h2>
                                <h2>Company Mail : {{$company->email}}</h2>
                                
                            </Div>
                        </div>
                        
                      
                    </div>
                </div>
                {{-- end --}}
             </div>
          </div>
       </div>
    </section>
    <!-- /.content -->
 </div>
@endsection




