@extends('admin.layouts.master')
@section('title','Edit Product')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="header-icon">
          <i class="fa fa-users"></i>
       </div>
       <div class="header-title">
          <h1>Edit Company</h1>
          <small>Edit Company</small>
       </div>
    </section>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
        
    <!-- Main content -->
    <section class="content">
       <div class="row">
          <!-- Form controls -->
          <div class="col-sm-12">
             <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                   <div class="btn-group" id="buttonlist"> 
                      <a class="btn btn-add " href="{{url('admin/view-product')}}"> 
                      <i class="fa fa-eye"></i>  View Companies </a>  
                   </div>
                </div>
                <div class="panel-body">
                    <form action="{{ route('employees.update',$employee->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="">First Name :</label>
                          <input type="text" name="first_name" id="" class="form-control" placeholder="Enter your First name" value="{{$employee->firstname}}" >
                          </div>
                          <div class="form-group">
                             <label for="">Last Name :</label>
                           <input type="text" name="last_name" id="" class="form-control" placeholder="Enter your Last name" value="{{$employee->lastname}}" >
                           </div>
                          <div class="form-group">
                            <label for="">Email :</label>
                            <input type="text" name="email" id="" class="form-control" placeholder="Enter your email" value="{{$employee->email}}" >
                          </div>
                          <div class="form-group">
                             <label for="">Select Company :</label>
                             <select name="companyid" class="form-control" id="">
                                 @foreach ($comp as $companies)
                                     <option value="{{$companies['id']}}">{{$companies['name']}}</option>
                                 @endforeach
                             </select>
                          </div>
                             <div class="form-group">
                                 <label for="">Phone Number # :</label>
                                 <input type="text" name="phone_number" autocomplete="off" id="" class="form-control" placeholder="Enter your phone" value="{{$employee->phone}}" >
                               </div>
                           
          
                        <br>
                        <input type="submit" value="Submit Form" class="btn btn-info btn-lg">
                
                    </form>
                </div>
             </div>
          </div>
       </div>
    </section>
    <!-- /.content -->
 </div>
@endsection