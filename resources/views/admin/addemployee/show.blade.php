@extends('admin.layouts.master')
@section('title',' Employee Details')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="header-icon">
          <i class="fa fa-users"></i>
       </div>
       <div class="header-title">
          <h1>Employee Detials</h1>
          <small>Employee Detials</small>
       </div>
    </section>
    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
    <!-- Main content -->
    <section class="content">
     
       <div class="row">
          <!-- Form controls -->
          <div class="col-sm-12">
             <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                   <div class="btn-group" id="buttonlist"> 
                   <a class="btn btn-add " href="{{route('employees.index')}}"> 
                      <i class="fa fa-eye"></i> Back To Employee View </a>  
                   </div>
                </div>
                <div class="panel-body">
                    <div class="jumbotron bg-info text-center">
                        <h3 class=" p-3">Welcome To Employee Detail</h3>
                        <div class="container">
                            <Div class="" style="background-color: grey; border-radius: 5px; padding: 20px; margin-top: 5px; border: 2px solid red;">
                                <h2>Employee First Name : {{$employee->fistname}}</h2>
                                <h2>Employee Last Name : {{$employee->lastname}}</h2>
                                <h2>Employee Mail : {{$employee->email}}</h2>
                                <div class="form-group">
                                    <label for="">Employees  :</label>
                                    <select name="companyid" id="">
                                        @foreach ($comp as $companies)
                                            <option value="{{$companies['id']}}">{{$companies['name']}}</option>
                                        @endforeach
                                    </select>
                                 </div>
                                <h2>Company Mail : {{$employee->phone}}</h2>
                            
                                
                            </Div>
                        </div>
                        
                      
                    </div>
                </div>
                {{-- end --}}
             </div>
          </div>
       </div>
    </section>
    <!-- /.content -->
 </div>
@endsection

