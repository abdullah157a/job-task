@extends('admin.layouts.master')
@section('title','View Company')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="header-icon">
          <i class="fa fa-product-hunt"></i>
       </div>
       <div class="header-title">
          <h1>View Employee</h1>
          <small>Employee List</small>
       </div>
    </section>
    <!-- Main content -->
    <section class="content">
       <div class="row">
          <div class="col-sm-12">
             <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                   <div class="btn-group" id="buttonexport">
                      <a href="#">
                         <h4>View Employee</h4>
                      </a>
                   </div>
                </div>
                <div class="panel-body">
                <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                @if (session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
            @if (session()->has('error'))
                <div class="alert alert-danger">
                    {{session()->get('error')}}
                </div>
            @endif

                   <div class="btn-group">
                      <div class="buttonexport" id="buttonlist"> 
                         <a class="btn btn-add" href="{{ route('employees.create') }}"> <i class="fa fa-plus"></i> Add Employee
                         </a>  
                      </div>
                   </div>
                   <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                   <div class="table-responsive">
                      <table id="table_id" class="table table-bordered table-striped table-hover">
                         <thead>
                            <tr class="info">
                                
                               <th>Fist Name</th>
                               <th>Last NAme</th>
                               <th>Email</th>
                               <th>Company Name</th>
                               <th>Phone</th>
                               <th>Show</th>
                               <th>Edit</th>
                               <th>Delete</th>

                            </tr>
                         </thead>
                         <tbody>

                             @foreach ($comp as $item)
                             <tr>
                                <td>{{$item->firstname}}</td>
                                <td>{{$item->lastname}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->company->name}}</td>
                                <td>{{$item->phone}}</td>

                                <td>
                                 <a href="{{ route('employees.show',$item->id) }}" class="btn btn-primary">Show</a>
                             </td>

                                <td>
                                    <a href="{{ route('employees.edit',$item->id) }}" class="btn btn-success">Edit</a>
                                </td>
                                 <td>
                                    <form action="{{ route('employees.destroy',$item->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>
                                </td>
                                
                               
                             </tr>
                             @endforeach
                          
                            
                         </tbody>
                      </table>
                      <div class="d-flex justify-content-center align-items-center">
                        <div>{{$comp->links()}}</div>
                    </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <!-- customer Modal1 -->
     
       <!-- /.modal -->
    </section>
    <!-- /.content -->
 </div> 
@endsection