<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\storerequest;
use App\Http\Requests\editcom;
use Image;
use File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function index()
    {
        $comp = Company::latest()->paginate(2);
        return view('admin.addcompany.index', compact('comp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.addcompany.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(storerequest $request)
    {
        $company = new Company;
        $company->name = $request->name;
        $company->email = $request->email;
        //$company->website = $request->website_url;
        $images = $request->file('image');
        if ($request->hasFile('image')) :
            $ImageUpload = Image::make($images);
            $originalPath = storage_path('app/public/company/');
            File::exists($originalPath) or File::makeDirectory($originalPath, 0777, true, true);
            $ImageName = time() . '-' . $images->getClientOriginalName();
            $ImageUpload->resize(100, 100);
            $ImageUpload->save($originalPath . $ImageName);
        else :
            $ImageName = '';
        endif;
        $company->logo  = $ImageName;
        $company->website = $request->web_url;
        // dd($company);
        $company->save();
        return redirect('companies')->with('success', 'Company inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('admin.addcompany.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.addcompany.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(editcom $request, Company $company)
    {
        $images = $request->file('image');
        if ($request->hasFile('image')) :
            $ImageUpload = Image::make($images);
            $originalPath = storage_path('app/public/company/');
            File::exists($originalPath) or File::makeDirectory($originalPath, 0777, true, true);
            $ImageName = time() . '-' . $images->getClientOriginalName();
            $ImageUpload->resize(200, 130);
            $ImageUpload->save($originalPath . $ImageName);
            $company->update([
                "name" => $request->name,
                "email" => $request->email,
                "website" => $request->web_url,
                "logo" => $ImageName,

            ]);
        else :
            $company->update([
                "name" => $request->name,
                "email" => $request->email,
                "website" => $request->web_url,
            ]);
        endif;
        return redirect()->route('companies.index')->with('success', 'Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return redirect()->route('companies.index')->with('error', 'Data Deleted Successfully');
    }
}
