<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function authentication(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
                return redirect('companies');
            } else {
                return redirect('/')->with('flash_message_error', 'invalid username or password');
            }
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/')->with('flash_message_success', 'you are logout out');
    }
}
