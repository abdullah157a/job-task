<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Company;
use App\Http\Requests\empstore;
use App\Http\Requests\editemp;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comp = Employee::with('company')->latest()->paginate(2);
        return view('admin.addemployee.index', compact('comp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $comp = Company::all();
        return view('admin.addemployee.create', compact('comp'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(empstore $request)
    {
        Employee::create([
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'company_id' => $request->companyid,
            'email' => $request->email,
            'phone' => $request->phone_number,
        ]);
        return redirect('employees')->with('success', 'Company inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $comp = Company::all();
        return view('admin.addemployee.show', compact('employee', 'comp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $comp = Company::all();
        return view('admin.addemployee.edit', compact('employee', 'comp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(editemp $request, Employee $employee)
    {
        $employee->update([
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'company_id' => $request->companyid,
            'email' => $request->email,
            'phone' => $request->phone_number,
        ]);
        return redirect('employees')->with('success', 'Updated  successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return redirect()->route('employees.index')->with('error', 'Data Deleted Successfully');
    }
}
