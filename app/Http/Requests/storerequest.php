<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class storerequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            "name" => "required",
            "email" => "required|email|unique:companies",
            "image" => "required|image|max:7048",

        ];
    }
}
